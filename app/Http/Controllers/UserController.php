<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{

    /**
     * Aqui vai validar os dados apenas da primeira etapa
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function validateFirstStep(
        Request $request
    )
    {
        try {
            $this->validate($request, [
                'name'  => 'required',
                'email' => 'required|email'
            ]);

            return response()->json([
                'error' => false
            ], 200);

        } catch (ValidationException $e) {
            return response()
                ->json($e->errors(), 400);
        }
    }

    /**
     * Aqui vai validar todos os dados obrigatórios para finalizar o cadastro
     *
     * @param UserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(
        UserRequest $request
    )
    {
        return response()->json([
            'error' => false
        ], 200);
    }

}
